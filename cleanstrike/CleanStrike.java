package cleanstrike;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author test
 */
public class CleanStrike {

    /**
     * @param args the command line arguments
     */
    static Board b;
    static Scanner scanner;

    public static void main(String[] args) {
        // TODO code application logic here
        
        b = new Board();
        System.out.println("||...Clean Strike...||");
        
        System.out.println("Enter the number of players : ");
        
        scanner = new Scanner(System.in);
        int noOfPlayers = scanner.nextInt();
        //scanner.close();
        
        Player[] players = new Player[noOfPlayers];
        
        for(int i = 0; i<noOfPlayers; i++) {
        	int num = i+1;
        	players[i] = new Player("Player " + num);
        }
        
//        Player p1 = new Player("Player 1");
//        Player p2 = new Player("Player 2");

//        System.out.println(String.format("%s starts the game !!", p1.name));
        System.out.println(String.format("%s starts the game !!", players[0].name));

//        int i = 0;
//        do {
//            if (i % 2 == 0) {
//                playersTurn(p1);
//            } else {
//                playersTurn(p2);
//            }
//            i++;
//        } while (b.getCoins() != 0 || b.getRed() != 0);
        
        do {
        	for(int p = 0; p<noOfPlayers && (b.getCoins() != 0 || b.getRed() != 0); p++) {
        		playersTurn(players[p]);
        	}
        } while(b.getCoins() != 0 || b.getRed() != 0);
        
//        System.out.println(String.format("Game ended. Final score is %s - %s", p1.getScore(), p2.getScore()));
//        System.out.println(String.format("%s wins !!!", p1.getScore() > p2.getScore() ? p1.name : p2.name));
        scanner.close();
        System.out.println(String.format("Game ended. Final scores are"));
        
        for(int p = 0; p<noOfPlayers; p++) {
        	System.out.println(players[p].name + " : " + players[p].getScore());
        }
        
//        int p1Score = p1.getScore();
//        int p2Score = p2.getScore();
//        
//        if (p1Score > p2Score ? isGameWon(p1Score, p2Score) : isGameWon(p2Score, p1Score)) {
//            System.out.println(String.format("%s wins !!! ;)", p1Score > p2Score ? p1.name : p2.name));
//        } else {
//            System.out.println("Game is drawn. coz, no solid winner !! :)");
//        }
        
        Player winner = isGameWon(players);
        if(winner != null) {
        	System.out.println(String.format("%s wins", winner.name));
        } else {
        	System.out.println("Game is drawn. coz, no solid winner !! :)");
        }
        
    }
    
    static Player isGameWon(Player[] players) {
    	boolean gameWon = false;
    	Player winner = players[0];
    	for(int p = 0; p < players.length; p++) {
    		
    		if(p > 0) {
    			if(players[p].getScore() > winner.getScore()) {
        			if(players[p].getScore() >= 5 && players[p].getScore() - winner.getScore() > 3) {
        				gameWon = true;
        			}
        			winner = players[p];
        		}
    		}
    	}

		if(winner == players[0] && winner.getScore() >= 5) {
			gameWon = true;
		}
		
    	return gameWon ? winner : null;
    
    }
    
    static boolean isGameWon(int score1, int score2) {
        if(score1 >= 5 && score1 - score2 > 3) {     // winning score should be atleast 5 or 3 points higher that losing score.
            return true;
        }
        return false;
    }

    static void playersTurn(Player p) {

        System.out.println();
        System.out.println(String.format("%s's turn. Choose one from the following :", p.name));
        System.out.println("1. Strike");
        System.out.println("2. Multistrike");
        System.out.println("3. Red strike");
        System.out.println("4. Striker Strike");
        System.out.println("5. Defunct coin");
        System.out.println("6. None");
        System.out.println("> ");

        try {
            int stroke = scanner.nextInt();
            //scanner.close();
            boolean isStrikeAvailable = b.isStrikeAvailable();
            boolean isMultiStrikeAvailable = b.isMultiStrikeAvailable();
            boolean isRedStrikeAvailable = b.isRedStrikeAvailable();
            int strokePoint = 0;
                    
            switch (stroke) {

                case Stroke.STRIKE:
                    
                	if(isStrikeAvailable) {
                		//p.addScore(1);
                        strokePoint = 1;
                        b.reduceCoin(1);
                        System.out.println(String.format("%s scores 1 point !!", p.name));
                	} else {
                		System.out.println("Coins are not available. Only Red is there..");
                        playersTurn(p);
                	}
                    
                    break;

                case Stroke.MULTISTRIKE:
                    
                    if (isMultiStrikeAvailable) {
                        //p.addScore(2);
                        strokePoint = 2;
                        b.reduceCoin(2);    // TODO - all but 2 coins in ??
                        System.out.println(String.format("%s scores 2 points !!", p.name));
                    } else {
                        System.out.println("Multi Strike not available..");
                        playersTurn(p);
                    }
                    break;

                case Stroke.RED_STRIKE:
                    
                    if (isRedStrikeAvailable) {
                        //p.addScore(3);
                        strokePoint = 3;
                        b.reduceRed();
                        System.out.println(String.format("Red covered !! %s scores 3 points !!", p.name));
                    } else {
                        System.out.println("Red Strike not available..");
                        playersTurn(p);
                    }
                    break;

                case Stroke.STRIKER_STRIKE:
                    
                    //p.addScore(-1);
                    strokePoint = -1;
                    System.out.println(String.format("%s loses 1 point !!", p.name));
                    break;

                case Stroke.DEFUNCT_COIN:
                    
                	if(isStrikeAvailable) {
                		//p.addScore(-2);
                        strokePoint = -2;
                        p.addHistory(5);
                        b.reduceCoin(1);
                        System.out.println(String.format("%s loses 2 points !!", p.name));
                	} else {
                		System.out.println("Coins are not available. Only Red is there..");
                        playersTurn(p);
                	}
                    
                    break;

                case Stroke.NONE:
                    
                    if (p.getNoneStreak() == 2) {   // if 3 stokes continuously are none
                        //p.addScore(-1);
                        strokePoint = -1;
                        System.out.println(String.format("None for last 3 turns. %s loses 1 point !!", p.name));
                        p.resetNoneStreak();
                    } else {
                        System.out.println(String.format("No points in this turn !!"));
                        p.addNoneSteak();
                    }
                    break;

                default:
                    
                    System.out.println("wrong input :( Try again...");
                    playersTurn(p);

            }
            p.addScore(strokePoint);
            
            boolean strokeCondition = stroke >= Stroke.STRIKE && stroke <= Stroke.getMax();	// stroke should be valid (1 to 6)
            boolean strikeCondition = !(stroke == Stroke.STRIKE && !isStrikeAvailable);	// should not be coin strike when coins not there 
            boolean multiStrikeCondition = !(stroke == Stroke.MULTISTRIKE && !isMultiStrikeAvailable);	// should not be multistike when multistrike not available
            boolean redStrikeCondition = !(stroke == Stroke.RED_STRIKE && !isRedStrikeAvailable);	// should not be red strike when red strike not available
            
            if (strokeCondition && strikeCondition && multiStrikeCondition && redStrikeCondition) {
                System.out.println(String.format("Coins remaining : %s, Red remaining : %s", b.getCoins(), b.getRed()));
                
                p.addHistory(stroke);
                if(stroke != Stroke.NONE) {
                    p.resetNoneStreak();
                }
            }
            

        } catch (InputMismatchException e) {
            System.out.println("wrong input :( Try again...");
            playersTurn(p);
        }

    }

}
